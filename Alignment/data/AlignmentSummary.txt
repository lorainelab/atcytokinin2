CfRoBa.1
Left reads:
          Input     :  10867013
           Mapped   :  10328335 (95.0% of input)
            of these:    421520 ( 4.1%) have multiple alignments (5 have >20)
Right reads:
          Input     :  10867013
           Mapped   :   9936636 (91.4% of input)
            of these:    403085 ( 4.1%) have multiple alignments (5 have >20)
93.2% overall read mapping rate.

Aligned pairs:   9626083
     of these:    384635 ( 4.0%) have multiple alignments
                   14726 ( 0.2%) are discordant alignments
88.4% concordant pair alignment rate.
CfRoBa.2
Left reads:
          Input     :  30718102
           Mapped   :  29157335 (94.9% of input)
            of these:   1277102 ( 4.4%) have multiple alignments (22 have >20)
Right reads:
          Input     :  30718102
           Mapped   :  28418873 (92.5% of input)
            of these:   1237179 ( 4.4%) have multiple alignments (22 have >20)
93.7% overall read mapping rate.

Aligned pairs:  27593307
     of these:   1173595 ( 4.3%) have multiple alignments
                   50503 ( 0.2%) are discordant alignments
89.7% concordant pair alignment rate.
CfRoBa.3
Left reads:
          Input     :   6456913
           Mapped   :   6178686 (95.7% of input)
            of these:    155492 ( 2.5%) have multiple alignments (5 have >20)
Right reads:
          Input     :   6456913
           Mapped   :   6038108 (93.5% of input)
            of these:    152029 ( 2.5%) have multiple alignments (5 have >20)
94.6% overall read mapping rate.

Aligned pairs:   5850153
     of these:    145100 ( 2.5%) have multiple alignments
                    4904 ( 0.1%) are discordant alignments
90.5% concordant pair alignment rate.
CfRoDm.1
Left reads:
          Input     :   7835315
           Mapped   :   7426788 (94.8% of input)
            of these:    270035 ( 3.6%) have multiple alignments (12 have >20)
Right reads:
          Input     :   7835315
           Mapped   :   7082807 (90.4% of input)
            of these:    255551 ( 3.6%) have multiple alignments (12 have >20)
92.6% overall read mapping rate.

Aligned pairs:   6868437
     of these:    243376 ( 3.5%) have multiple alignments
                   10389 ( 0.2%) are discordant alignments
87.5% concordant pair alignment rate.
CfRoDm.2
Left reads:
          Input     :   6505259
           Mapped   :   6155727 (94.6% of input)
            of these:    318972 ( 5.2%) have multiple alignments (3 have >20)
Right reads:
          Input     :   6505259
           Mapped   :   6001011 (92.2% of input)
            of these:    307958 ( 5.1%) have multiple alignments (3 have >20)
93.4% overall read mapping rate.

Aligned pairs:   5814669
     of these:    291166 ( 5.0%) have multiple alignments
                   13261 ( 0.2%) are discordant alignments
89.2% concordant pair alignment rate.
CfRoDm.3
Left reads:
          Input     :   3502838
           Mapped   :   3340473 (95.4% of input)
            of these:     82104 ( 2.5%) have multiple alignments (1 have >20)
Right reads:
          Input     :   3502838
           Mapped   :   3264334 (93.2% of input)
            of these:     80085 ( 2.5%) have multiple alignments (1 have >20)
94.3% overall read mapping rate.

Aligned pairs:   3163902
     of these:     76383 ( 2.4%) have multiple alignments
                    2691 ( 0.1%) are discordant alignments
90.2% concordant pair alignment rate.
CfShBa.1
Left reads:
          Input     :   5373227
           Mapped   :   5164483 (96.1% of input)
            of these:    151482 ( 2.9%) have multiple alignments (7 have >20)
Right reads:
          Input     :   5373227
           Mapped   :   4946973 (92.1% of input)
            of these:    144004 ( 2.9%) have multiple alignments (7 have >20)
94.1% overall read mapping rate.

Aligned pairs:   4820157
     of these:    136503 ( 2.8%) have multiple alignments
                    6487 ( 0.1%) are discordant alignments
89.6% concordant pair alignment rate.
CfShBa.2
Left reads:
          Input     :  12163796
           Mapped   :  11660991 (95.9% of input)
            of these:    835262 ( 7.2%) have multiple alignments (4 have >20)
Right reads:
          Input     :  12163796
           Mapped   :  11353147 (93.3% of input)
            of these:    802460 ( 7.1%) have multiple alignments (4 have >20)
94.6% overall read mapping rate.

Aligned pairs:  11001626
     of these:    754274 ( 6.9%) have multiple alignments
                   42168 ( 0.4%) are discordant alignments
90.1% concordant pair alignment rate.
CfShBa.3
Left reads:
          Input     :  15220423
           Mapped   :  14619098 (96.0% of input)
            of these:    373957 ( 2.6%) have multiple alignments (10 have >20)
Right reads:
          Input     :  15220423
           Mapped   :  14286875 (93.9% of input)
            of these:    365074 ( 2.6%) have multiple alignments (10 have >20)
95.0% overall read mapping rate.

Aligned pairs:  13845239
     of these:    347809 ( 2.5%) have multiple alignments
                   12666 ( 0.1%) are discordant alignments
90.9% concordant pair alignment rate.
CfShDm.1
Left reads:
          Input     :   6079674
           Mapped   :   5789899 (95.2% of input)
            of these:    268695 ( 4.6%) have multiple alignments (1 have >20)
Right reads:
          Input     :   6079674
           Mapped   :   5556832 (91.4% of input)
            of these:    254479 ( 4.6%) have multiple alignments (1 have >20)
93.3% overall read mapping rate.

Aligned pairs:   5388401
     of these:    241770 ( 4.5%) have multiple alignments
                    9693 ( 0.2%) are discordant alignments
88.5% concordant pair alignment rate.
CfShDm.2
Left reads:
          Input     :  10848823
           Mapped   :  10389860 (95.8% of input)
            of these:    493759 ( 4.8%) have multiple alignments (5 have >20)
Right reads:
          Input     :  10848823
           Mapped   :  10089588 (93.0% of input)
            of these:    475884 ( 4.7%) have multiple alignments (5 have >20)
94.4% overall read mapping rate.

Aligned pairs:   9799421
     of these:    452099 ( 4.6%) have multiple alignments
                   18423 ( 0.2%) are discordant alignments
90.2% concordant pair alignment rate.
CfShDm.3
Left reads:
          Input     :   7945338
           Mapped   :   7613704 (95.8% of input)
            of these:    197485 ( 2.6%) have multiple alignments (2 have >20)
Right reads:
          Input     :   7945338
           Mapped   :   7448847 (93.8% of input)
            of these:    192576 ( 2.6%) have multiple alignments (2 have >20)
94.8% overall read mapping rate.

Aligned pairs:   7202431
     of these:    183126 ( 2.5%) have multiple alignments
                    6563 ( 0.1%) are discordant alignments
90.6% concordant pair alignment rate.
README.txt
WtRoBa.1
Left reads:
          Input     :   8534278
           Mapped   :   8120761 (95.2% of input)
            of these:    277555 ( 3.4%) have multiple alignments (6 have >20)
Right reads:
          Input     :   8534278
           Mapped   :   7788769 (91.3% of input)
            of these:    264501 ( 3.4%) have multiple alignments (6 have >20)
93.2% overall read mapping rate.

Aligned pairs:   7538131
     of these:    251553 ( 3.3%) have multiple alignments
                    9787 ( 0.1%) are discordant alignments
88.2% concordant pair alignment rate.
WtRoBa.2
Left reads:
          Input     :  13404357
           Mapped   :  12765040 (95.2% of input)
            of these:    642455 ( 5.0%) have multiple alignments (2 have >20)
Right reads:
          Input     :  13404357
           Mapped   :  12426537 (92.7% of input)
            of these:    620720 ( 5.0%) have multiple alignments (2 have >20)
94.0% overall read mapping rate.

Aligned pairs:  12046185
     of these:    585920 ( 4.9%) have multiple alignments
                   29341 ( 0.2%) are discordant alignments
89.6% concordant pair alignment rate.
WtRoBa.3
Left reads:
          Input     :   6707493
           Mapped   :   6427538 (95.8% of input)
            of these:    160864 ( 2.5%) have multiple alignments (3 have >20)
Right reads:
          Input     :   6707493
           Mapped   :   6274677 (93.5% of input)
            of these:    157089 ( 2.5%) have multiple alignments (3 have >20)
94.7% overall read mapping rate.

Aligned pairs:   6063196
     of these:    149429 ( 2.5%) have multiple alignments
                    5265 ( 0.1%) are discordant alignments
90.3% concordant pair alignment rate.
WtRoDm.1
Left reads:
          Input     :   9825441
           Mapped   :   9363662 (95.3% of input)
            of these:    289446 ( 3.1%) have multiple alignments (2 have >20)
Right reads:
          Input     :   9825441
           Mapped   :   8968480 (91.3% of input)
            of these:    275660 ( 3.1%) have multiple alignments (2 have >20)
93.3% overall read mapping rate.

Aligned pairs:   8684046
     of these:    262882 ( 3.0%) have multiple alignments
                    8671 ( 0.1%) are discordant alignments
88.3% concordant pair alignment rate.
WtRoDm.2
Left reads:
          Input     :   5315657
           Mapped   :   4926946 (92.7% of input)
            of these:    224246 ( 4.6%) have multiple alignments (2 have >20)
Right reads:
          Input     :   5315657
           Mapped   :   4776640 (89.9% of input)
            of these:    216378 ( 4.5%) have multiple alignments (2 have >20)
91.3% overall read mapping rate.

Aligned pairs:   4550715
     of these:    203522 ( 4.5%) have multiple alignments
                    8067 ( 0.2%) are discordant alignments
85.5% concordant pair alignment rate.
WtRoDm.3
Left reads:
          Input     :   5267526
           Mapped   :   5030827 (95.5% of input)
            of these:    125385 ( 2.5%) have multiple alignments (2 have >20)
Right reads:
          Input     :   5267526
           Mapped   :   4926073 (93.5% of input)
            of these:    122652 ( 2.5%) have multiple alignments (2 have >20)
94.5% overall read mapping rate.

Aligned pairs:   4749364
     of these:    117179 ( 2.5%) have multiple alignments
                    3179 ( 0.1%) are discordant alignments
90.1% concordant pair alignment rate.
WtShBa.1
Left reads:
          Input     :  11254693
           Mapped   :  10761643 (95.6% of input)
            of these:    799682 ( 7.4%) have multiple alignments (4 have >20)
Right reads:
          Input     :  11254693
           Mapped   :  10300300 (91.5% of input)
            of these:    753632 ( 7.3%) have multiple alignments (4 have >20)
93.6% overall read mapping rate.

Aligned pairs:   9979795
     of these:    716976 ( 7.2%) have multiple alignments
                   29998 ( 0.3%) are discordant alignments
88.4% concordant pair alignment rate.
WtShBa.2
Left reads:
          Input     :  13245869
           Mapped   :  12689760 (95.8% of input)
            of these:    609542 ( 4.8%) have multiple alignments (5 have >20)
Right reads:
          Input     :  13245869
           Mapped   :  12350335 (93.2% of input)
            of these:    589543 ( 4.8%) have multiple alignments (5 have >20)
94.5% overall read mapping rate.

Aligned pairs:  11991116
     of these:    558347 ( 4.7%) have multiple alignments
                   25715 ( 0.2%) are discordant alignments
90.3% concordant pair alignment rate.
WtShBa.3
Left reads:
          Input     :   5130790
           Mapped   :   4935642 (96.2% of input)
            of these:    120280 ( 2.4%) have multiple alignments (3 have >20)
Right reads:
          Input     :   5130790
           Mapped   :   4823127 (94.0% of input)
            of these:    117112 ( 2.4%) have multiple alignments (3 have >20)
95.1% overall read mapping rate.

Aligned pairs:   4673782
     of these:    111090 ( 2.4%) have multiple alignments
                    4909 ( 0.1%) are discordant alignments
91.0% concordant pair alignment rate.
WtShDm.1
Left reads:
          Input     :  10301467
           Mapped   :   9834894 (95.5% of input)
            of these:    531371 ( 5.4%) have multiple alignments (2 have >20)
Right reads:
          Input     :  10301467
           Mapped   :   9570620 (92.9% of input)
            of these:    512286 ( 5.4%) have multiple alignments (2 have >20)
94.2% overall read mapping rate.

Aligned pairs:   9291927
     of these:    484502 ( 5.2%) have multiple alignments
                   23840 ( 0.3%) are discordant alignments
90.0% concordant pair alignment rate.
WtShDm.2
Left reads:
          Input     :  15771000
           Mapped   :  15029562 (95.3% of input)
            of these:   1563418 (10.4%) have multiple alignments (3 have >20)
Right reads:
          Input     :  15771000
           Mapped   :  14393838 (91.3% of input)
            of these:   1464687 (10.2%) have multiple alignments (3 have >20)
93.3% overall read mapping rate.

Aligned pairs:  13880377
     of these:   1375001 ( 9.9%) have multiple alignments
                   80418 ( 0.6%) are discordant alignments
87.5% concordant pair alignment rate.
WtShDm.3
Left reads:
          Input     :   4700944
           Mapped   :   4513238 (96.0% of input)
            of these:    114954 ( 2.5%) have multiple alignments (3 have >20)
Right reads:
          Input     :   4700944
           Mapped   :   4418570 (94.0% of input)
            of these:    111973 ( 2.5%) have multiple alignments (3 have >20)
95.0% overall read mapping rate.

Aligned pairs:   4286937
     of these:    106890 ( 2.5%) have multiple alignments
                    3505 ( 0.1%) are discordant alignments
91.1% concordant pair alignment rate.
callRunTophatPaired.sh
logs
runTophatPaired.pbs
