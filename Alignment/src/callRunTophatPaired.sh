#!/bin/bash

# Run this script from the alignment directory.

SRC="."
FILES=$(ls ../fastq/nicknames/*_1.fastq*)

for FASTQ1 in $FILES
do
  FASTQ2=${FASTQ1/_1.fastq/_2.fastq}
  export FASTQ1
  export FASTQ2
  sample=${FASTQ1%_1.fastq*}
  sample=${sample#../fastq/nicknames/}
  qsub -e logs/$sample.log -o logs/$sample.log -v FASTQ1,FASTQ2 -N TH-$sample $SRC/runTophatPaired.pbs
done
