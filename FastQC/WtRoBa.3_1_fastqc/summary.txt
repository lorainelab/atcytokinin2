PASS	Basic Statistics	WtRoBa.3_1.fastq.gz
PASS	Per base sequence quality	WtRoBa.3_1.fastq.gz
PASS	Per sequence quality scores	WtRoBa.3_1.fastq.gz
FAIL	Per base sequence content	WtRoBa.3_1.fastq.gz
FAIL	Per base GC content	WtRoBa.3_1.fastq.gz
PASS	Per sequence GC content	WtRoBa.3_1.fastq.gz
PASS	Per base N content	WtRoBa.3_1.fastq.gz
PASS	Sequence Length Distribution	WtRoBa.3_1.fastq.gz
FAIL	Sequence Duplication Levels	WtRoBa.3_1.fastq.gz
PASS	Overrepresented sequences	WtRoBa.3_1.fastq.gz
WARN	Kmer Content	WtRoBa.3_1.fastq.gz
