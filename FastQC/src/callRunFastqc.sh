#!/bin/bash

# Run this script from the FastQC directory of one of the studies.

SRC="."
FILES=$(ls ../fastq/nicknames/*.fastq*)
JOBS=""

for f in $FILES
do
  export f
  sample=${f%.fastq*}
  sample=${sample#../fastq/nicknames/}
  JOBid=$(qsub -e logs/$sample.log -o logs/$sample.log -v f -N FastQC-$sample $SRC/runFastqc.pbs)
  JOBS=$JOBS:$JOBid
done

# after all of these jobs are done run the following:
# for file in $(ls ./*fastqc.zip); do f=${file%.zip}; echo $f; grep Duplication $f/summary.txt; grep Duplicate $f/fastqc_data.txt; done > DuplicationSummary.txt
# this is done using another script
# The job to run is set to wait for the above jobs to complete

qsub -e logs/summary.log -o logs/summary.log -N summary -W depend=afterok$JOBS $SRC/summarizeDups.pbs
