PASS	Basic Statistics	WtRoDm.2_2.fastq.gz
PASS	Per base sequence quality	WtRoDm.2_2.fastq.gz
PASS	Per sequence quality scores	WtRoDm.2_2.fastq.gz
FAIL	Per base sequence content	WtRoDm.2_2.fastq.gz
FAIL	Per base GC content	WtRoDm.2_2.fastq.gz
PASS	Per sequence GC content	WtRoDm.2_2.fastq.gz
PASS	Per base N content	WtRoDm.2_2.fastq.gz
PASS	Sequence Length Distribution	WtRoDm.2_2.fastq.gz
WARN	Sequence Duplication Levels	WtRoDm.2_2.fastq.gz
WARN	Overrepresented sequences	WtRoDm.2_2.fastq.gz
WARN	Kmer Content	WtRoDm.2_2.fastq.gz
