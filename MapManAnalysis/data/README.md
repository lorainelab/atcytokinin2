# Data used for MapMan Analysis #

* * *

The MapManAnalysis mark down expect to find a file called Ath_AGI_LOCUS_TAIR10_Aug2012.txt in this folder.  This file is copyrighted, and cannot be included in the repository, however you can download a copy for free from http://mapman.gabipd.org/web/guest/mapmanstore and add it to this directory if you want to re-run the pipeline on your own machine.
