# Explore Differential Gene Expression with MapMan #


* * *

# What's here 


* * *

## Data 

The MapManAnalysis markdown expects to find a file called Ath_AGI_LOCUS_TAIR10_Aug2012.txt in this folder.  This file is copyrighted, and cannot be included in the repository, however you can download a copy for free from http://mapman.gabipd.org/web/guest/mapmanstore
and add it to this directory if you want to re-run the pipeline on your own machine.

* * *

## MapManAnalysis

This markdown uses GOSeq to identify MapMan categories with unusually many DE genes.

* * *

## MapManImages

This folder holds the images generated using the MapMan desktop application.

The images were generated using MapMan Version 3.5.1 (19.11.2010) which is (Version 10).

The mapping used was the Ath_AGI_TAIR9_Jan2010 which was included with the software.
The "Experimental Data" was from the files DiffExprAnalysis/results/WtRo.DE-0.05.txt, and WtSh.DE-0.05.txt. 

Options were tweaked so that the points were as large as possible without overlaping the labels (sizes 5-8). The scale was -3 to 3. Red is used for positive fold changes, and blue for negative.  Most of the blocks had to be reshaped to make this size work. All of the tweaking was done with the root data and transfered to the shoots data (double click the shoot data file at the left) and the root data was tweaked to use the same parameters that were used in the corresponding image in the rice project (separate repository).  

Images were exported to be 160 mm wide (~6 inches) and 600 dpi. Dispite the high resolution, the labels still look pixilated.  I think MapMan has an image with labels as the background; the symbols can be exported at higher resultions but the background will not look as smooth.

* * *

## src

This folder contains R scripts.    

 * DrawGOterms.R is called in VisualizeEnrichment.Rmd to create the bar charts.
 * MapMan.R is called in MapManAnalysis.Rmd to modify the gene ids in the MapMan file.

* * *

## VisualizeEnrichment

This markdown creates bar charts showing MapMan categories with unusually many DE genes. Uses output from MapManAnalysis.

