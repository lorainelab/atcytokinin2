### Project Wide variables

getFDR = function(){
  return(0.05)
}

getMinLog2FoldChange = function(){
  return(0)
}

# Use this function to get gene descriptions from the TAIR 10 bed file
# genes - optional, a vector of genes to get descriptions for
getAnnots = function(genes=NULL){
  fname='../ExternalDataSets/data/TAIR10.bed.gz'
  annots = read.delim(fname, header=F)[,c(4,14)]
  names(annots) = c("gene", "desc")
  annots$gene = gsub("\\.[^a]$", "", annots$gene)
  annots = annots[!duplicated(annots$gene),]
  row.names(annots) = annots$gene
  if(!is.null(genes)){
    annots = annots[genes, "desc"]
  }
  return(annots)
}

# Use this function to set a consistent set of colors across multiple figures.
# set - vector of colomn names or other variables being assigned a color.
# Color scheme taken from the RColorBrewer package using: brewer.pal(8, "Paired")
getColors = function(set=NULL){
  colors = c("#A6CEE3", "#1F78B4", "#B2DF8A", "#33A02C", "#FB9A99", "#E31A1C", "#FDBF6F","#FF7F00")
  names(colors) = c("WtRoDm", "WtRoBa", "WtShDm", "WtShBa", "CfRoDm", "CfRoBa", "CfShDm", "CfShBa")
  if (!is.null(set)){
    newset=set
    for (gr in names(colors)){
      newset[grep(gr, set)] = gr
    }
  colors = colors[newset]
  names(colors)=set
  }
  return(colors)
}


getRawCounts = function(){
  fname='../Counts/data/WtCountsPerGene.txt'
  d = read.delim(fname, header=T, sep='\t', comment.char="#")
  names(d) = gsub("...alignment_Processed.", "", names(d))
  names(d) = gsub(".bam", "", names(d))
  row.names(d) = d$Geneid
  return(d)
}

# get differential expression results for BA vs mock-treated roots
getDeResultsRoots=function(){
  fname='../DiffGeneExpr/results/WtRo.txt.gz'
  d=read.table(fname,as.is=T,quote='"',header=T,sep='\t')
  row.names(d)=d$gene
  return(d)
}

# get differential expression results for BA vs mock-treated shoots
getDeResultsShoots=function(){
  fname='../DiffGeneExpr/results/WtSh.txt.gz'
  d=read.table(fname,as.is=T,quote='"',header=T,sep='\t')
  row.names(d)=d$gene
  return(d)
}


# Returns named vector containing largest transcript (spliced, in kb)
# per locus.
getTranscriptSizes=function(fname='../ExternalDataSets/results/tx_size.txt.gz') {
  d=read.table(fname,as.is=T,header=T)
  sizes=d$size/1000
  names(sizes)=d$gene
  return(sizes)
}

getFPKM = function(fname='../Counts/results/FPKM.txt.gz'){
  fpkm = read.delim2(fname, stringsAsFactors=F)
  last=ncol(fpkm)
  fpkm[2:last] = sapply(fpkm[2:last], as.numeric)
  return(fpkm)
}