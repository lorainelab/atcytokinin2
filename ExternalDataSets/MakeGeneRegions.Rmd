---
title: "Make SAG file of gene regions"
author: "Ivory Clabaugh Blakley"
date: "March 5, 2015"
output:
  html_document:
    toc: true
---
**Date run: `r date()`**

# Introduction

[FeatureCounts](http://bioinf.wehi.edu.au/featureCounts/) counts reads or read pairs that overlap a gene or other feature. FeatureCounts accepts a simple format called SAF which consists of 

* gene name
* chromosome name
* start 
* end
* strand

In this Markdown:

* Create SAF file for Arabidopsis TAIR 10 annotations
* Count genes and  variants per gene

Analysis
--------

Read the file with gene models and get columns we need to define gene regions. Also, read BED file for transgenes.

```{r}
f='data/TAIR10.bed.gz'
annots=read.delim(f,header=F,as.is=T,sep='\t')
annots=annots[,c(1,2,3,4,6)]
names(annots)=c('seq','start','end',"locus",'strand')

# convert the uniqe locus ids into more general gene ids
annots$locus = gsub("\\.[^a]$", "", annots$locus)

#Get the earliest start position for each locus
o=order(annots$seq,annots$start)
annots=annots[o,]
minstarts=annots[!duplicated(annots$locus),
                 c('locus','seq','start')]

#Get the latest end position for each locus
o=order(annots$seq,annots$end,decreasing=T)
annots=annots[o,]
maxends=annots[!duplicated(annots$locus),
               c('locus','end','strand')]

saf=merge(minstarts,maxends,by='locus')
saf=saf[,c('locus','seq','start','end','strand')]
lens=saf$end-saf$start
names(lens)=saf$locus
saf$end=saf$end+1 # saf is one-based 
```

There were `r nrow(annots)` gene model annotations representing `r nrow(saf)` genes.

The largest gene had size of `r max(lens)` and the smallest gene had size of `r min(lens)`. 

Examine the distribution of sizes:

```{r fig.width=7,fig.height=5}
par(las=1)
main="Histogram of gene sizes"
ylab="base pairs"
hist(log10(lens))
```

Write the SAF file to disk in SAF format.

```{r}
f1='results/TAIR10_SAF_for_featureCount.tsv'
write.table(saf,file=f1,col.names=F,row.names=F,
            sep='\t',quote=F)
```

Conclusion
----------

There were `r nrow(annots)` gene model annotations representing `r nrow(saf)` genes.

Limitations 
------------

* This assumes that gene models with the same locus name leading up to the "." are from the same gene.

Session Info
------------

```{r}
sessionInfo()
```
