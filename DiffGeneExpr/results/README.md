# What's here 

This folder contains output of Markdown files residing in the parent directory.

Outputs include image and plain text files, typically tab-delimited
files used in subsequent analyses or visualized in tools like AraCyc
Omics viewer or MapMan.

* FdrHist-roots.png - made by DeAnalysis.Rmd

Other files are from DiffGeneExpr.Rmd
