#!/bin/bash

# Run this script from the countsPerGene directory.

module load subread

SRC="."

# Make a counts file for the Wt samples


BAMS=$(ls ../alignment_Processed/Wt*.bam)
OUTDIR="WtCountsPerGene"
export BAMS OUTDIR
  qsub -e logs/Wt.log -o logs/Wt.log -v BAMS,OUTDIR -N FC.Wt $SRC/runFeatureCounts.pbs



# Make a counts file for the Cf samples

# BAMS=$(ls ../alignment_Processed/Cf*.bam)
# export BAMS
# qsub -e logs/Cf.log -o logs/Cf.log -v BAMS -N FC.Cf $SRC/runFeatureCounts.pbs
