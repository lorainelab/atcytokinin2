# Sample Names Records

## AtCytoNames.xlsx
This excel file contains three sheets.    
 * FastqFileNames - the copied from an email from Tracy, explaining which files corresponded to which genotype/tissue/treatment.
 * AtCytoNames - where I used find/replace and concatenate to make the shorter nicknames for each file
 * MakePointerNames - where I used concatenate to make an appropriate pointer name for each file
 * Pointers - Sheet listing the pointer or nickname for each file alongside the original file name.  This sheet was exported as FastqNickNames.txt
 
## FastqNickNames.txt
Lists the original fastq file name, and the nickname that was used for it.  This nickname was used to make sample file names in subsequent steps.

## MakeLinks.sh
The script was used to make a series of pointers so that data processing scripts and their output and logs could all use the short informative name for each sample and still access the correct fastq file, while never loosing track of which original file what which.